# unmountVolumes

Unmount any /Volumes when putting mac to sleep not named Machintosh*


## sleepwatcher

Thanks to Bernhard Baehr for putting `sleepwatcher` out there for
us all to use.

Sleepwatcher runs in the background and automatically runs `~/.sleep` when
your computer goes to sleep and runs `~/.wakeup` when it wakes up (open the
lid).  Newer versions of sleepwatcher should be available here:

   http://www.bernhard-baehr.de


## Installation:

In a terminal windows, where you downloaded this repo, execute this command:

```
sh ./install.sh
```

The install.sh script will install the following files:

- ~/.sleep       - sleepwatcher will run this when you close the lid
- ~/.wakeup      - sleepwatcher will run this when you open the lid
- sleepwatcher   - binary tool (2.2.1) to do something when you open lid
- sleepwatcher.8 - man page for sleepwatcher
- de.bernhard-baehr.sleepwatcher.plist - plist for sleepwatcher

That's it.

The file `~/.sleep` looks at all mounted /Volumes and will attempt to unmount then when you close the lid.

By default the file `~/.wakeup` does nothing. You can tailor that file to do most anything you wish.

