#!/bin/sh

if [ -f ~/Library/LaunchAgents/de.bernhard-baehr.sleepwatcher.plist ]; then \
   launchctl unload ~/Library/LaunchAgents/de.bernhard-baehr.sleepwatcher.plist; \
   rm ~/Library/LaunchAgents/de.bernhard-baehr.sleepwatcher.plist; \
fi
echo removing sleepwatcher binary and man page
sudo rm -f /usr/local/sbin/sleepwatcher /usr/local/share/man/man8/sleepwatcher.8
echo removing sleepwatcher control files
rm -f ~/.sleep

