#!/bin/sh

ADIR="~/Library/LaunchAgents"
SDIR="/usr/local/sbin"
MDIR="/usr/local/share/man/man8"

sudo install -d ${SDIR} ${MDIR}
sudo install -Cv sleepwatcher ${SDIR}
sudo install -Cv sleepwatcher.8 ${MDIR}
sudo install -Cv de.bernhard-baehr.sleepwatcher.plist ~/Library/LaunchAgents
install -Cv -b -m 0755 .sleep .wakeup ~
launchctl load ~/Library/LaunchAgents/de.bernhard-baehr.sleepwatcher.plist
